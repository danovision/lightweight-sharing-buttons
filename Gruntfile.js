module.exports = function(grunt) {

    // Project configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        compass: {
            dev: {
                options: {
                    config: 'config.rb',
                    environment: 'development'
                }
            }
        },

        connect: {
            dev: {
                options: {
                    base: 'src',
                    livereload: true,
                    open: true
                }
            }
        },

        csscomb: {
            sass: {
                files: {
                    'src/css/sass/lws.scss': ['src/css/sass/lws.scss']
                }
            }
        },

        watch: {
            options: {
                interrupt: true,
                livereload: true
            },
            dev: {
                files: ['src/css/**/*.scss', 'src/*.html'],
                tasks: ['compass:dev']
            }
        }
    });


    // Load plugins and tasks
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-csscomb');


    // Define custom tasks
    grunt.registerTask('default', ['compass:dev', 'connect:dev', 'watch:dev']);
};