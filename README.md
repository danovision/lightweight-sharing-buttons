# Lightweight Share Buttons

## Overview

- Reduce HTTP requests
- Reusable CSS that is easily skinned
- IE8+ compatible

NB the Lightwight Share prefix `'lws-'` is used to avoid conflicts with other classes on the page.

Alternative Facebook link for apps:

    https://www.facebook.com/dialog/feed?app_id=<facebookAppId>&link=<URL>&redirect_uri=<URL>&display=popup&picture=<image-url>&name=<TITLE>&caption=<comment>&description=<description>

## Reference

    https://gist.github.com/vindia/2901160

    http://georgovassilis.blogspot.co.uk/2013/09/replacing-social-sharing-buttons-with.html

## Art

Logos generated at

    http://socicon.com/generator.html