var Util = Util || {};

Util = (function () {
    'use strict';
    function removeClass(element, cls) {
        var re;
        if (typeof element.classList !== 'undefined') {
            element.classList.remove(cls);
            return element;
        } else if (element.className.indexOf(cls) !== -1) {
            re = new RegExp('\\b' + cls + '\\b', 'g');
            element.className = element.className.replace(re, '');
            return element;
        }
    }

    function addClass(element, cls) {
        element.className += ' ' + cls;
    }

    return {
        removeClass: removeClass,
        addClass: addClass
    };
}());