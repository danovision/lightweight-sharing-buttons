// CONSTRUCTOR STARTS //////////////////////////////////////////////////////
// initObj.block = a div.lws element
function LWShare(initObj) {
    'use strict';


    // VARIABLE DECLARATIONS ///////////////////////////////////////////////////
    var classnamePrefix,
        classnameOpen,
        classnameToggleButton,
        classnameSharelist,
        blockEl,
        toggleBtn,
        sharelistEl,
        isOpen,
        instance,   // pass `this` to event handlers
        my;         // use `my` for instance vars that need to be visible from inside prototype methods
    // END OF VARIABLE DECLARATIONS ////////////////////////////////////////////

    
    // ensure that new has been used
    if (!(this instanceof LWShare)) {
        // console.log('LWShare not called with new!');
        return new LWShare(initObj);
    }
    

    // VARIABLE DEFINITIONS ////////////////////////////////////////////////////

    // Configuration
    blockEl               = initObj.block;
    classnamePrefix       = initObj.cnPrefix    || 'lws';
    classnameOpen         = initObj.cnOpen      || classnamePrefix + '-sharelist--open';
    classnameToggleButton = initObj.cnToggle    || classnamePrefix + '-toggle--button';
    classnameSharelist    = initObj.cnShareList || classnamePrefix + '-sharelist';
    isOpen                = initObj.startOpen   || false;
    instance              = this;
    my                    = {};    // initialise
    
    // END OF VARIABLE DEFINITIONS /////////////////////////////////////////////


    // Instance initialisation

    if (!blockEl) {
        // console.log('No LWS block found! blockEl: ', blockEl);
        return;
    }

    toggleBtn = blockEl.querySelector('.' + classnameToggleButton);
    if (toggleBtn) {
        toggleBtn.onclick = function () {
            LWShare.prototype.toggleIcons.call(instance);
        };
        sharelistEl = blockEl.querySelector('.' + classnameSharelist);
    } else {
        // console.log('No LWS toggle button found! toggleBtn: ', blockEl);
        return;
    }

    
    // expose selected properties in one namespace called 'my' (eg for use with prototype methods)
    my.blockEl = blockEl;
    my.sharelistEl = sharelistEl;
    my.classnameOpen = classnameOpen;
    my.isOpen = isOpen;
    this.my = my;
    

    // for clarity, explicitly return `this` /////////////////////////////////////
    // (JS returns `this` anyway, but let's remember it happens) /////////////////
    return this;
    
}

// CONSTRUCTOR ENDS ////////////////////////////////////////////////////////////




// PROTOTYPE METHODS ///////////////////////////////////////////////////////////

// create functions to add to class's prototype
(function () {
    'use strict';

    var objProto = LWShare.prototype;

    objProto.init = function (selector) {
        var i,
            els;

        els = document.querySelectorAll(selector);
        for (i = 0; i < els.length; i += 1) {
            /* jshint -W031 */
            new LWShare({block: els[i]});
        }
    };

    objProto.revealIcons = function (my) {
        Util.addClass(my.sharelistEl, my.classnameOpen);
    };

    objProto.hideIcons = function (my) {
        Util.removeClass(my.sharelistEl, my.classnameOpen);
    };

    objProto.toggleIcons = function () {
        if (this.my.isOpen === true) {
            objProto.hideIcons(this.my);
        } else {
            objProto.revealIcons(this.my);
        }
        this.my.isOpen = !this.my.isOpen;
    };
}());

// PROTOTYPE METHODS END ///////////////////////////////////////////////////////
